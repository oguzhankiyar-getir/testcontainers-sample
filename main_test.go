package main

import (
	"context"
	"fmt"
	"github.com/docker/go-connections/nat"
	"github.com/stretchr/testify/assert"
	"github.com/testcontainers/testcontainers-go"
	"github.com/testcontainers/testcontainers-go/wait"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"strconv"
	"testing"
	"time"
)

func Test(t *testing.T) {
	mongoC, host, port, err := StartMongoContainer(context.Background())
	if err != nil {
		log.Fatalf("Failed to create mongo client: %v", err)
	}
	defer mongoC.Terminate(context.Background())

	mongoURI := fmt.Sprintf("mongodb://%s:%d/?connect=direct", host, port)

	client, err := mongo.NewClient(options.Client().ApplyURI(mongoURI))
	if err != nil {
		log.Fatalf("Failed to create mongo client: %v", err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	client.Connect(ctx)

	collection := client.Database("testdatabase").Collection("my-collection")
	result, err := collection.InsertOne(context.Background(), map[string]string{
		"name": "oguzhan",
	})

	assert.NotNil(t, result)
	assert.NotEmpty(t, result.InsertedID)
	assert.Nil(t, err)
}

func StartMongoContainer(ctx context.Context) (testcontainers.Container, string, int, error) {
	mongoPort, _ := nat.NewPort("", strconv.Itoa(27017))

	env := make(map[string]string)

	timeout := time.Second * 30
	if int64(timeout) < 1 {
		timeout = 5 * time.Minute // Default timeout
	}

	req := testcontainers.ContainerRequest{
		Image:        "mongo:4.4.3",
		Env:          env,
		ExposedPorts: []string{string(mongoPort)},
		SkipReaper: true,
		WaitingFor:   wait.ForLog("Waiting for connections").WithStartupTimeout(timeout),
	}
	var mongoC testcontainers.Container
	mongoC, err := testcontainers.GenericContainer(ctx, testcontainers.GenericContainerRequest{
		ContainerRequest: req,
		Started:          true,
	})
	if err != nil {
		err := fmt.Errorf("Failed to start mongo container: %v", err)
		return nil, "", 0, err
	}

	host, err := mongoC.Host(ctx)
	if err != nil {
		err = fmt.Errorf("Failed to get mongo container host: %v", err)
		return nil, "", 0, err
	}

	port, err := mongoC.MappedPort(ctx, mongoPort)
	if err != nil {
		err = fmt.Errorf("Failed to get exposed mongo container port: %v", err)
		return nil, "", 0, err
	}

	return mongoC, host, port.Int(), nil
}